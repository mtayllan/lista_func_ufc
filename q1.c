#include <stdio.h>

void printIntegerToBin(int num){
	int quociente = num;
	int qtd=0, binary[16];
	int i;
	for(qtd = 0; quociente>=1; qtd++, quociente/=2){
		binary[qtd] = quociente%2;
	}
	
	for(i=qtd-1; i>=0; i--){
		printf("%d", binary[i]);
	}
}

int main(void) {	
	int num;
	scanf("%d", &num);
	printIntegerToBin(num);
  	return 0;
}