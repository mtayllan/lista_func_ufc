#include <stdio.h>
#include <string.h>

void troca(char *vetor, int i, int j);
void printVetor(char *vetor, int size);

int total = 1;
int count = 1;

int permut(char *str, int k) {
	int i, len;
	len = strlen(str);
	if (k == len) {
		if (count == total) {
			printf("%s", str);
		} else {
			printf("%s, ", str);
			count++;
		}
	} else {
		for (i = k; i < len; i++) {
			troca(str, k, i);
			permut(str, k + 1);
			troca(str, i, k);
		}
	}
}

int main(void) {
	int n;
	scanf("%d", &n);
	char letters[n];
	int i;
	for (i = 0; i < n; i++)
		letters[i] = i + 'A';
	letters[i] = '\0';
	for (i = 1; i <= n; i++)
		total *= i;
	permut(letters, 0);
	return 0;
}

void printVetor(char *vetor, int size) {
	int i;
	for (i = 0; i < size; i++) {
		printf("%c", vetor[i]);
	}
}

void troca(char *vetor, int i, int j) {
	char aux = vetor[i];
	vetor[i] = vetor[j];
	vetor[j] = aux;
}