#include <stdio.h>

int mult(int n1, int n2, int control, int resultado){
	if(n2 != 0){
		if(control!=n1){
			return mult(n1,n2,control+1,resultado);
		}else{
			return mult(n1,n2-1,0, resultado + control);
		}
	}else{
		return resultado;
	}
}

int main(){
    int m,n;
    scanf("%d%d", &m, &n);
    printf("%d", mult(m,n,0,0));
    return 0;
}

