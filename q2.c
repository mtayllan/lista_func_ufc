#include <stdio.h>

int mult(int n1, int n2, int result){
	if(n2 != 0){
		return mult(n1, n2-1, result+n1);
	}else{
		return result;
	}
}

int main(void) {
  
	int n1,n2;
	scanf("%d %d", &n1, &n2);
	printf("%d", mult(n1,n2,0));

  return 0;
}