#include <stdio.h>

int sum(int n1, int n2){
	if(n2 != 0){
		return sum(n1+1,n2-1);
	}else{
		return n1;
	}
}

int main(void) {
  
	int n1,n2;
	scanf("%d %d", &n1, &n2);
	printf("%d", sum(n1,n2));

  return 0;
}