#include <stdio.h>

int main(void){
    int value, max=0;
    while(scanf("%d ", &value) == 1){
        if(max==0)max = value;
        if(value>max)max = value;
    }
    printf("%d", max);
    return 0;
}